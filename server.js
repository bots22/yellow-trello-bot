require('dotenv').config()
const { connectToServer } = require('./src/server/init')
const { connectToBot } = require('./src/bot/bot');

connectToServer();
connectToBot();
