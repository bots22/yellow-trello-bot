const dotenv = require('dotenv');
dotenv.config();

const { USERNAME: username, PASSWORD: password, DATABASE: database, HOST: host } = process.env;

module.exports = {
  development: {
    username,
    password,
    database,
    host: host || 'localhost',
    dialect: 'postgres',
  },
  test: {
    username: 'root',
    password: null,
    database: 'database_test',
    host: '127.0.0.1',
    dialect: 'mysql',
  },
  production: {
    username,
    password,
    database,
    host: host || 'db',
    dialect: 'postgres',
  },
};
