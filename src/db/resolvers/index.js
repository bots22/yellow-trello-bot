const { getUserId } = require('./usersResolver');

module.exports = {
    getUserId
}
