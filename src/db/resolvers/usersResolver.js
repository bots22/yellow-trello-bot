const { Users, Chats } = require('../models');

async function getUserId(ctx, from, chat) {
    const { username, first_name: firstName, last_name: lastName, id: telegramId } = from;
    const { id: chatId } = chat;
    let user = await Users.findOne({ where: { telegramId } });

    if (!user) {
        user = await Users.create({ username, firstName, lastName, telegramId });
    }

    const chatText = chatId.toString();

    await Chats.findOrCreate({
            where: { chatId: chatText },
            defaults: {
                userId: user.userId,
                chatId: chatText
            }},
    );

    return user;
}

module.exports = {
    getUserId
}
