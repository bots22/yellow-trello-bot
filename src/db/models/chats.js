const Sequelize = require('sequelize');
const sequelize = require('../config').sequelize;

const Chats = sequelize.define('Chats', {
  chatIdToken: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  userId: Sequelize.INTEGER,
  chatId: Sequelize.STRING,

  createdAt: { type: Sequelize.DATE },
  updatedAt: { type: Sequelize.DATE },
}, {
  schema: 'public',
  tableName: 'chats',
  timestamps: true,
  underscored: true
});

module.exports = {
  Chats
};

