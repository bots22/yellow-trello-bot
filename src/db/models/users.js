const Sequelize = require('sequelize');
const sequelize = require('../config').sequelize;

const Users = sequelize.define('Users', {
  userId: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  username: Sequelize.STRING,
  firstName: Sequelize.STRING,
  lastName: Sequelize.STRING,
  phone: Sequelize.STRING,
  telegramId: Sequelize.STRING,

  createdAt: { type: Sequelize.DATE },
  updatedAt: { type: Sequelize.DATE },
}, {
  schema: 'public',
  tableName: 'users',
  timestamps: true,
  underscored: true
});

module.exports = {
  Users
};

