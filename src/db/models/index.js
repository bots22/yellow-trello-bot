const { Users } = require('./users');
const { Chats } = require('./chats');

module.exports = {
    Chats,
    Users
}
