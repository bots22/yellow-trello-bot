const { Telegraf, session } = require('telegraf');
const { botFunctions } = require('./functions');
const botToken = process.env.BOT_TOKEN;

console.log('Bot started.')

const bot = new Telegraf(botToken);
const connectToBot = () => {
    bot.use(session());
    botFunctions(bot);
    bot.launch();

    return bot;
};

module.exports = {
    connectToBot
}
