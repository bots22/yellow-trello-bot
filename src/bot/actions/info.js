const { getUserId } = require('../../db/resolvers');
const startMessage = (bot) => {
    bot.start(async (ctx) => {
        const { from, chat } = ctx.update.message;
        const user = await getUserId(ctx, from, chat);
        ctx.reply(`Вітаю, ${user.lastName} ${user.firstName}`, {
            reply_markup: {
                remove_keyboard: true
            }
        });
    });
    return bot;
}

module.exports = {
    startMessage
}
