const axios = require('axios');

const { API_KEY: apiKey,
        API_TOKEN: apiToken,
        BOARD_ID: boardId,
        WEBHOOK_URL: webhookUrl,
        ID_MODEL: idModel
    } = process.env;

const connectToTrelloBoard = async() => {
    try {
        const webhookExists = await checkWebhookExists();
        const callbackURL =`${webhookUrl}/webhook`;
        if (!webhookExists) {
            const data = {
                key: apiKey,
                token: apiToken,
                idModel: idModel,
                callbackURL,
            };

            const response = await axios.post('https://api.trello.com/1/webhooks', data);
            console.log('Webhook created:', response.data);
        } else {
            console.log('Webhook already exists');
        }
    } catch (error) {
        console.error('Error creating webhook:', error.response.data);
    }
}

const checkWebhookExists = async () => {
    try {
        const response = await axios.get(`https://api.trello.com/1/tokens/${apiToken}/webhooks?key=${apiKey}`);
        const webhooks = response.data;

        for (const webhook of webhooks) {
            if (webhook.callbackURL === webhookUrl && webhook.idModel === idModel) {
                return true;
            }
        }

        return false;
    } catch (error) {
        console.error('Error checking webhooks:', error.response.data);
        throw error;
    }
};

module.exports = {
    connectToTrelloBoard
}
