const { startMessage } = require('./info');
const {
    connectToTrelloBoard,
} = require('./commands');

module.exports = {
    startMessage,
    connectToTrelloBoard
}
