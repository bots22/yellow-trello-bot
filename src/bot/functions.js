const {
    startMessage,
    connectToTrelloBoard
} = require('./actions');

const botFunctions = (bot) => {
    startMessage(bot);
    connectToTrelloBoard();
};

module.exports = {
    botFunctions
}
