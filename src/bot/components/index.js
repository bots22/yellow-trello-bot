const { keyboard, mainKeyboard, emptyTableKeyboard, inlineKeyboardSearch } = require('./keyboard');

module.exports = {
    keyboard,
    emptyTableKeyboard,
    inlineKeyboardSearch,
    mainKeyboard
}
