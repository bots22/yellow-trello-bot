const express = require('express');
const axios = require('axios');
const bodyParser = require('body-parser');
const { Users, Chats } = require("../db/models");

const { BOT_TOKEN: botToken } = process.env;

const app = express();
const port = 3000;
const connectToServer = () => {
    app.use(bodyParser.json());

    app.post('/webhook', (req, res) => {
        handleWebhook(req.body);
        res.sendStatus(200);
    });

    app.get('/webhook', (req, res) => {
        res.sendStatus(200);
    });

    const sendToTelegramMsg = async (text) => {
        const chats = await Chats.findAll();
        for (const { chatId } of chats) {
            try {
                const response = await axios.post(`https://api.telegram.org/bot${botToken}/sendMessage`, {
                    chat_id: chatId,
                    text
                });
                console.log('Message sent to Telegram successfully:', response.data);
            } catch (error) {
                console.error('Error sending message to Telegram:', error);
            }
        }
    }
    async function handleWebhook(webhookData) {
        if (webhookData.action.type === 'updateCard' && webhookData.action.data) {
            console.log('webhookData.action.data', webhookData.action.data)
            const { card, listBefore, listAfter } =  webhookData.action.data;
            if (listBefore && listAfter) {
                const cardName = card.name;
                const listBeforeName = listBefore.name;
                const listAfterName = listAfter.name;
                const text = `${cardName} was moved from ${listBeforeName} to ${listAfterName}`;
                await sendToTelegramMsg(text);
            }
        }
    }

    app.listen(port, () => {
        console.log(`Server is listening at http://localhost:${port}`);
    });
}

module.exports = {
    connectToServer
};
